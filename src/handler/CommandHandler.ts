import { Message } from "discord.js"
import config from '../config/config'
import {CommandBase} from "../commands/CommandBase";
import {CommandContext} from "../commands/CommandContext";

export class CommandHandler {

    private static _instance: CommandHandler
    private _registry: { [key:string]: CommandBase }

    constructor() {
        CommandHandler._instance = this
        this._registry = {}
    }

    static getInstance(): CommandHandler {
        return CommandHandler._instance != undefined ? CommandHandler._instance : new CommandHandler()
    }

    async handleCommand(message: Message): Promise<void> {
        if(message.author.bot || !message.content.startsWith(config.prefix)) return undefined

        let context: CommandContext = new CommandContext(message, config.prefix)

        if(Object.keys(this._registry).includes(context.getCommandName())) {
            this._registry[context.getCommandName()].run(context)
        } else {
            this._registry["help"].run(context, `The command ${context.getCommandName()} doesn't exist`)
        }
    }

    registerCommand(command: CommandBase): void {
        this._registry[command.getName()] = command
    }

    getCommandRegistry(): { [key:string]: CommandBase } {
        return this._registry
    }
}
