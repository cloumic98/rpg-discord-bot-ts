import { Client, Intents } from "discord.js";
import * as dotenv from 'dotenv'
import {CommandHandler} from "./handler/CommandHandler";
import {HelpCommand} from "./commands/HelpCommand";

dotenv.config()

const client = new Client({intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES
]})

client.on("ready", () => {
    console.log("Registering commands")

    new HelpCommand()

    console.log("The bot is ready")
})

client.on("messageCreate", (message) => {
    CommandHandler.getInstance().handleCommand(message)
})

client.login(process.env.TOKEN).then()
