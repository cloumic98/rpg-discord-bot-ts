import {Message} from "discord.js";
import {ArgsParser} from "./CommandBase";

export class CommandContext {

    private _command: string;
    private _args: string[];
    private _message: Message;
    private _commandPrefix: string;

    constructor(message: Message, prefix: string) {
        const args = message.content.slice(prefix.length).trim().split(/ +/g)

        this._command = args.shift()!.toLowerCase()
        this._args = args
        this._message = message
        this._commandPrefix = prefix
    }

    getCommandName(): string {
        return this._command.toLowerCase()
    }

    getArgs(): string[] {
        return this._args
    }

    getMessage(): Message {
        return this._message
    }

    getPrefix(): string {
        return this._commandPrefix
    }

    applyArgsType(type: ArgsParser[]): any[] {
        let response: any[] = []

        this._args.forEach((args, index) => {

            switch(type[index]) {
                case ArgsParser.String:
                    response.push(args as string)
                    break
                case ArgsParser.Integer:
                    response.push(args as any as number)
                    break
                default:
                    response.push(args)
                    break
            }
        })

        return response
    }
}
