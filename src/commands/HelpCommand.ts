import {ArgsParser, CommandBase} from "./CommandBase"
import {MessageEmbed} from "discord.js"
import {CommandHandler} from "../handler/CommandHandler"
import {CommandContext} from "./CommandContext"

export class HelpCommand extends CommandBase {

    constructor() {
        super("help", "Command list")
    }

    async run(context: CommandContext, error?: string): Promise<void> {
        if(error != undefined) {
            context.getMessage().channel.send(error)
        }

        let embed = new MessageEmbed()
            .setTitle("Help Section")
            .setColor("DARK_PURPLE")

        Object.entries(CommandHandler.getInstance().getCommandRegistry()).forEach(([key, value]) => {
            embed.addField(value.getName(), value.getDescription())
        })

        embed.setFooter({ text: context.getArgs().join(", ") })

        context.getMessage().channel.send({ embeds: [ embed ] })
    }

    argsParser(): ArgsParser[] {
        return [ArgsParser.String]
    }
}
