import {CommandHandler} from "../handler/CommandHandler";
import {CommandContext} from "./CommandContext";

export abstract class CommandBase {

    private _name: string
    private _description: string

    constructor(name: string, description: string) {
        this._name = name
        this._description = description
        this.register()
    }

    getName(): string {
        return this._name
    }

    getDescription(): string {
        return this._description
    }

    register(): void {
        CommandHandler.getInstance().registerCommand(this)
        console.log(this.getName(), "has been registered !")
    }

    abstract run(context: CommandContext, error?:string): void

    abstract argsParser(): ArgsParser[]
}

export enum ArgsParser {
    String,
    Integer,
    User
}
